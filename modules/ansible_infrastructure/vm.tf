resource "azurerm_virtual_machine" "test_servers" {
  count                         = var.count_servers_test
  location                      = azurerm_resource_group.ansible_group.location
  name                          = "${var.name}_test${count.index + 1}"
  resource_group_name           = azurerm_resource_group.ansible_group.name
  network_interface_ids         = [element(azurerm_network_interface.test_servers.*.id, count.index)]
  vm_size                       = var.vm_size
  delete_os_disk_on_termination = true


  storage_image_reference {
    publisher = var.storage_image_reference["publisher"]
    offer     = var.storage_image_reference["offer"]
    sku       = var.storage_image_reference["sku"]
    version   = var.storage_image_reference["version"]
  }


  storage_os_disk {
    name              = "${var.name}test${count.index + 1}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.name}est${count.index + 1}"
    admin_username = var.admin_user
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.admin_user}/.ssh/authorized_keys"
      key_data = tls_private_key.ssh_ansible.public_key_openssh
    }

  }
  tags = {
    name = "${var.name}_test${count.index + 1}"
  }
}


resource "azurerm_virtual_machine" "master_servers" {
  location                      = azurerm_resource_group.ansible_group.location
  name                          = "${var.name}_master"
  resource_group_name           = azurerm_resource_group.ansible_group.name
  network_interface_ids         = [azurerm_network_interface.master_int.id]
  vm_size                       = var.vm_size
  delete_os_disk_on_termination = true
  depends_on                    = [tls_private_key.ssh_ansible, azurerm_virtual_machine.test_servers]
  


  storage_image_reference {
    publisher = var.storage_image_reference["publisher"]
    offer     = var.storage_image_reference["offer"]
    sku       = var.storage_image_reference["sku"]
    version   = var.storage_image_reference["version"]
  }


  storage_os_disk {
    name              = "${var.name}master"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"

  }

  os_profile {
    computer_name  = "${var.name}master"
    admin_username = var.admin_user
    custom_data = <<-EOF
      #!/bin/bash
      sudo apt -y update
      sudo apt -y install mc
      sudo apt -y install pip
      sudo apt -y install ansible-core 
      sudo apt -y install ansible 
      sudo apt -y install tree
      EOF
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/${var.admin_user}/.ssh/authorized_keys"
      key_data = tls_private_key.ssh_ansible.public_key_openssh
    }
  }

  provisioner "file" {
    source      = var.filename
    destination = "/home/${var.admin_user}/.ssh/key.pem"

    connection {
      type        = "ssh"
      user        = var.admin_user
      private_key = tls_private_key.ssh_ansible.private_key_pem
      host        = azurerm_public_ip.master_public_ip.ip_address
    }

  }
  provisioner "remote-exec" {
    inline = [
      "sudo chmod 400 /home/${var.admin_user}/.ssh/key.pem",
      "mkdir /home/${var.admin_user}/ansible"
    ]

    connection {
      type        = "ssh"
      user        = var.admin_user
      private_key = tls_private_key.ssh_ansible.private_key_pem
      host        = azurerm_public_ip.master_public_ip.ip_address
    }
  }

  provisioner "file" {
    source      = "./${var.ansiblecfg}"
    destination = "/home/${var.admin_user}/ansible/ansible.cfg"

    connection {
      type        = "ssh"
      user        = var.admin_user
      private_key = tls_private_key.ssh_ansible.private_key_pem
      host        = azurerm_public_ip.master_public_ip.ip_address
    }
  }
  provisioner "file" {
    source      = "./${var.hosts}"
    destination = "/home/${var.admin_user}/ansible/hosts.txt"

    connection {
      type        = "ssh"
      user        = var.admin_user
      private_key = tls_private_key.ssh_ansible.private_key_pem
      host        = azurerm_public_ip.master_public_ip.ip_address
    }
  }
  tags = {
    name = "${var.name}_master"
  }
}



