resource "local_file" "ansiblecfg" {
  filename        = var.ansiblecfg
  file_permission = "666"
  content         = <<-EOT
[defaults] 
host_key_checking = false
inventory= ./hosts.txt
EOT
}
