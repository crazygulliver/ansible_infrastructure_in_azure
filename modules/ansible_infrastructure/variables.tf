variable "name" {
  default = "ansible"

}

variable "admin_user" {
  default = "ansible"
}

variable "owner" {
  default = "Administrator"
}

variable "count_servers_test" {
  default = 4
}

variable "vm_size" {
  default = "Standard_DS1_v2"

}

variable "storage_image_reference" {
  type = map(any)
  default = {
    "publisher" = "Canonical"
    "offer"     = "0001-com-ubuntu-server-jammy"
    "sku"       = "22_04-lts"
    "version"   = "latest"
  }
}

variable "location" {
  default = "North Europe"
}

variable "filename" {
  default = "key.pem"
}

variable "hosts" {
  default = "hosts.txt"
}

variable "ansiblecfg" {
  default = "ansible.cfg"
}