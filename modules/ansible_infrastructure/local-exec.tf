
resource "local_file" "keys" {
  content  = tls_private_key.ssh_ansible.private_key_pem
  filename = var.filename
}