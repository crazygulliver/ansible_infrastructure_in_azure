resource "azurerm_network_interface" "test_servers" {
  count               = 4
  name                = "${var.name}_test${count.index + 1}"
  location            = azurerm_resource_group.ansible_group.location
  resource_group_name = azurerm_resource_group.ansible_group.name

  ip_configuration {
    name                          = "${var.name}-config${count.index + 1}"
    subnet_id                     = azurerm_subnet.test_subnets.id
    private_ip_address_allocation = "Dynamic"   

  }
}


resource "azurerm_subnet" "test_subnets" {
  name                 = "${var.name}_test"
  resource_group_name  = azurerm_resource_group.ansible_group.name
  virtual_network_name = azurerm_virtual_network.test_network.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_virtual_network" "test_network" {
  name                = "${var.name}_test"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.ansible_group.location
  resource_group_name = azurerm_resource_group.ansible_group.name
}


resource "azurerm_network_interface" "master_int" {
  name                = "${var.name}_master"
  location            = azurerm_resource_group.ansible_group.location
  resource_group_name = azurerm_resource_group.ansible_group.name

  ip_configuration {
    name                          = "${var.name}-master-config"
    subnet_id                     = azurerm_subnet.master_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.master_public_ip.id

  }
}


resource "azurerm_network_interface_security_group_association" "master_association" {
  network_interface_id      = azurerm_network_interface.master_int.id
  network_security_group_id = azurerm_network_security_group.master_sg.id
}


resource "azurerm_subnet" "master_subnet" {
  name                 = "${var.name}_master"
  resource_group_name  = azurerm_resource_group.ansible_group.name
  virtual_network_name = azurerm_virtual_network.test_network.name
  address_prefixes     = ["10.0.1.0/29"]
}


resource "azurerm_public_ip" "master_public_ip" {
  name                = "${var.name}-master-${terraform.workspace}"
  resource_group_name = azurerm_resource_group.ansible_group.name
  location            = azurerm_resource_group.ansible_group.location
  allocation_method   = "Static"
  sku                 = "Standard"
  tags = {
    Owner      = var.owner
  }
}


resource "azurerm_network_security_group" "master_sg" {
  name                = "${var.name}-master-${terraform.workspace}"
  resource_group_name = azurerm_resource_group.ansible_group.name
  location            = azurerm_resource_group.ansible_group.location

  security_rule {
    name                       = "${var.name}-inbound"
    description                = "Inbound"
    direction                  = "Inbound"
    priority                   = 100
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  tags = {
    Owner      = var.owner
  }
}



