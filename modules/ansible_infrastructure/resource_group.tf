resource "azurerm_resource_group" "ansible_group" {
  name     = "${var.name}-test"
  location = var.location
}