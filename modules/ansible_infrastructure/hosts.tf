resource "local_file" "hosts" {
  filename = var.hosts
  file_permission = "666"
  content  = <<-EOT
[staging_servers]

${var.name}1 ansible_host=${azurerm_network_interface.test_servers.0.private_ip_address}


[prod_servers]

${var.name}2 ansible_host=${azurerm_network_interface.test_servers.1.private_ip_address}
${var.name}3 ansible_host=${azurerm_network_interface.test_servers.2.private_ip_address}
${var.name}4 ansible_host=${azurerm_network_interface.test_servers.3.private_ip_address}


[all_servers:children]
staging_servers
prod_servers


[all_servers:vars]
ansible_user=${var.admin_user}
ansible_ssh_private_key_file=/home/${var.admin_user}/.ssh/key.pem
EOT
}