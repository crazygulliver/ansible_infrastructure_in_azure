output "public_ip" {
  value = azurerm_public_ip.master_public_ip.ip_address

}

output "private_ips" {
  value = { for k, v in azurerm_network_interface.test_servers : k + 1 => v.private_ip_address }

}

output "user_name" {
  value = var.admin_user
}

