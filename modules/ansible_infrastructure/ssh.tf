resource "tls_private_key" "ssh_ansible" {
  algorithm = "RSA"
  rsa_bits = 4096
}